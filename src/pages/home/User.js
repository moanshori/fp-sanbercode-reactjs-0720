import React from "react";
import {
    Typography
} from '@material-ui/core/';

function User (props) {
    return(
        <Typography variant="h6">
            {props.name}
        </Typography>
    )
}

export default User
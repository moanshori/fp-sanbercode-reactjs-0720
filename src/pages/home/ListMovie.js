import React, { useState, useEffect } from "react"
import axios from 'axios'
import {
    Typography,
    Paper,
    Box,
    List,
    ListItem,
    ListItemIcon,
    ListItemAvatar,
    ListItemText,
    makeStyles,
    ListItemSecondaryAction,
    IconButton,
    Grid,
    CardContent,
    Card,
    CardMedia
} from '@material-ui/core/';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import MovieIcon from '@material-ui/icons/Movie';
import ImageIcon from '@material-ui/icons/Image';
import StarIcon from '@material-ui/icons/Star';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import DescriptionIcon from '@material-ui/icons/Description';
import RateReviewIcon from '@material-ui/icons/RateReview';
import TitleIcon from '@material-ui/icons/Title';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SortIcon from '@material-ui/icons/Sort';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        whiteSpace: 'nowrap',
        marginBottom: theme.spacing(1),
    },
}));

function ListMovie() {
    const classes = useStyles();
    const [dataMovie, setDataMovie] = useState([])
    const [movieTitle, setmovieTitle] = useState("")
    const [movieDesc, setmovieDesc] = useState("")
    const [movieYear, setmovieYear] = useState("")
    const [movieDura, setmovieDura] = useState("")
    const [movieGenre, setmovieGenre] = useState("")
    const [movieRating, setmovieRating] = useState("")
    const [movieReview, setmovieReview] = useState("")
    const [movieImage, setmovieImage] = useState("")
    const [show, setShow] = useState(false)

    const handleMoreMovie = (event) => {
        console.log(event.currentTarget.value)
        setShow(true)

        axios.get(`https://backendexample.sanbersy.com/api/movies/${event.currentTarget.value}`)
            .then(res => {
                console.log("get signle data")
                setmovieTitle(res.data.title)
                setmovieDesc(res.data.description)
                setmovieDura(res.data.duration)
                setmovieGenre(res.data.genre)
                setmovieRating(res.data.rating)
                setmovieReview(res.data.review)
                setmovieYear(res.data.year)
                setmovieImage(res.data.image_url)
            });
    }

    useEffect(() => {
        if (dataMovie.length == 0) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    setDataMovie(res.data.map(item => {
                        return {
                            id: item.id,
                            title: item.title,
                            description: item.description,
                            year: item.year,
                            duration: item.duration,
                            genre: item.genre,
                            rating: item.rating,
                            review: item.review,
                            image_url: item.image_url
                        }
                    }));
                });
        }
    }, [dataMovie])

    return (
        <Box mt="50px" mr="10px" ml="10px">
            <Typography variant="h6" align="left">
                Movie List
                    </Typography>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Paper variant="outlined" elevation={1} className={classes.paper}>
                        <List dense="true" style={{ maxHeight: 450, overflow: 'auto' }}>
                            {dataMovie.map((row) => (
                                <div>
                                    <ListItem>
                                        <ListItemAvatar>
                                            <MovieIcon style={{ color: "blue" }} />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={`${row.title} (${row.year})`}
                                            secondary={row.genre}
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" aria-label="more" value={row.id} onClick={handleMoreMovie}>
                                                <MoreVertIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                </div>
                            ))}
                        </List>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Card variant="outlined" elevation={1} style={{ maxHeight: 480, overflow: 'auto' }}>
                        {show == true && (
                            <>
                                <Box ml="10px" mt="10px">
                                    <Typography variant="body2" align="left" color="textSecondary">
                                        Movie Details
                                    </Typography>
                                </Box>
                                <CardContent>
                                    <CardMedia component={'img'}
                                        image={movieImage}>
                                    </CardMedia>
                                    <List dense="true">
                                        <ListItem>
                                            <ListItemIcon>
                                                <TitleIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieTitle}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <CalendarTodayIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieYear}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <SortIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieGenre}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <StarIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieRating}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <AccessTimeIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieDura}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <DescriptionIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieDesc}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <RateReviewIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={movieReview}
                                            />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </>
                        )}
                        {show == false && (
                            <Box m="20px">
                                <Typography variant="body2" align="left" color="textSecondary" padding="15px">
                                    Select one of the lists beside to see the details.
                            </Typography>
                            </Box>
                        )}
                    </Card>
                </Grid>
            </Grid>
        </Box >
    )
}

export default ListMovie
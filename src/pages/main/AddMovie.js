import React, { useState } from "react"
import axios from 'axios';
import {
    Container,
    Box,
    Paper,
    InputAdornment,
    TextField,
    Grid,
    MenuItem,
    Snackbar,
    Button,
    Typography
} from '@material-ui/core/';
import MuiAlert from '@material-ui/lab/Alert';
import ImageIcon from '@material-ui/icons/Image';
import StarIcon from '@material-ui/icons/Star';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import DescriptionIcon from '@material-ui/icons/Description';
import RateReviewIcon from '@material-ui/icons/RateReview';
import TitleIcon from '@material-ui/icons/Title';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SortIcon from '@material-ui/icons/Sort';
import SaveIcon from '@material-ui/icons/Save';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AddMovie = () => {
    const [movieTitle, setmovieTitle] = useState("")
    const [movieDesc, setmovieDesc] = useState("")
    const [movieYear, setmovieYear] = useState("")
    const [movieDura, setmovieDura] = useState("")
    const [movieGenre, setmovieGenre] = useState("")
    const [movieRating, setmovieRating] = useState("")
    const [movieReview, setmovieReview] = useState("")
    const [movieImage, setmovieImage] = useState("")
    const [success, setSuccess] = useState(false);

    const years = Array.from(new Array(50), (val, index) => new Date().getFullYear() - index);
    const ratings = Array.from(new Array(10), (val, index) => index + 1);

    const handleChangeTitle = (event) => {
        setmovieTitle(event.target.value);
    }
    const handleChangeGenre = (event) => {
        setmovieGenre(event.target.value);
    }
    const handleChangeYear = (event) => {
        setmovieYear(event.target.value);
    }
    const handleChangeRating = (event) => {
        setmovieRating(event.target.value);
    }
    const handleChangeDuration = (event) => {
        setmovieDura(event.target.value);
    }
    const handleChangeDesc = (event) => {
        setmovieDesc(event.target.value);
    }
    const handleChangeReview = (event) => {
        setmovieReview(event.target.value);
    }
    const handleChangeImage = (event) => {
        setmovieImage(event.target.value);
    }

    const handleAdd = () => {
        console.log("adding data")
        let data = {
            title: movieTitle,
            description: movieDesc,
            year: movieYear,
            duration: movieDura,
            genre: movieGenre,
            rating: movieRating,
            review: movieReview,
            image_url: movieImage
        }
        console.log(data)
        axios.post(`https://backendexample.sanbersy.com/api/movies`, data)
            .then(res => {
                console.log("Add new movie")
                console.log(res.data)
                setSuccess(true)
            })
        setmovieTitle("")
        setmovieDesc("")
        setmovieDura("")
        setmovieGenre("")
        setmovieImage("")
        setmovieRating("")
        setmovieYear("")
        setmovieReview("")
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccess(false);
    };
    
    return (
        <Container fixed>
            <Box mt="80px" mr="20px" ml="20px">
                <Paper variant="outlined" elevation={1} >
                    <div style={{ marginTop: "25px" }}>
                        <Typography variant="h5" component="h2">
                            Form Create Movie Data
                    </Typography>
                    </div>
                    <Grid container spacing={1} justify="center" style={{ marginTop: "10px", marginBottom: "20px" }}>
                        <Grid item xs={6} >
                            <div style={{ marginTop: "20px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Title:"
                                    value={movieTitle}
                                    onChange={handleChangeTitle}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <TitleIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6} >
                            <div style={{ marginTop: "20px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Genre:"
                                    value={movieGenre}
                                    onChange={handleChangeGenre}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SortIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Year:"
                                    select
                                    value={movieYear}
                                    onChange={handleChangeYear}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <CalendarTodayIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {years.map((year) => (
                                        <MenuItem value={year}>
                                            {year}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Rating:"
                                    select
                                    value={movieRating}
                                    onChange={handleChangeRating}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <StarIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {ratings.map((rate) => (
                                        <MenuItem value={rate}>
                                            {rate}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Duration:"
                                    value={movieDura}
                                    onChange={handleChangeDuration}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <AccessTimeIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Description:"
                                    value={movieDesc}
                                    onChange={handleChangeDesc}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <DescriptionIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Review:"
                                    value={movieReview}
                                    onChange={handleChangeReview}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <RateReviewIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Image Url:"
                                    value={movieImage}
                                    onChange={handleChangeImage}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <ImageIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div style={{ margin: "10px", marginRight: "50px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <Button
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={handleAdd}
                                    startIcon={<SaveIcon />}>
                                    Submit
                                    </Button>
                            </div>
                        </Grid>
                    </Grid>
                </Paper>
                <Snackbar open={success} autoHideDuration={1000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        New data sucessfully added.
                    </Alert>
                </Snackbar>
            </Box>
        </Container>
    )
}

export default AddMovie
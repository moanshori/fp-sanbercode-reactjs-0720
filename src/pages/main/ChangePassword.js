import React, { useState, useContext, useEffect } from 'react';
import { Button, InputAdornment, Container, Box, TextField, Typography, Paper, Snackbar } from '@material-ui/core/';
import { LoginContext } from '../../context/LoginContext';
import MuiAlert from '@material-ui/lab/Alert';
import axios from 'axios';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';

import {
    useHistory
} from "react-router-dom";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ChangePassword = () => {
    let history = useHistory();
    const [isLogin, setisLogin] = useContext(LoginContext);
    const [inputOldPasswd, setinputOldPasswd] = useState("")
    const [inputNewPasswd, setinputNewPasswd] = useState("")
    const [inputRetypeNewPasswd, setinputRetypeNewPasswd] = useState("")
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);

    const handleChangeOldPasswd = (event) => {
        setinputOldPasswd(event.target.value);
    }
    const handleChangeNewPasswd = (event) => {
        setinputNewPasswd(event.target.value);
    }
    const handleChangeRetypeNewPasswd = (event) => {
        setinputRetypeNewPasswd(event.target.value);
    }

    const handleUpdatePasswd = () => {
        if (inputOldPasswd != "" && inputNewPasswd != "" && inputRetypeNewPasswd != "") {
            if (inputOldPasswd == isLogin.password && inputOldPasswd != inputNewPasswd && inputNewPasswd == inputRetypeNewPasswd) {
                axios.put(`https://backendexample.sanbersy.com/api/users/${isLogin.id}`, { username: isLogin.username, password: inputNewPasswd })
                    .then(res => {
                        // console.log("edit")
                        // console.log(res.data);
                    })
                console.log("sukses ganti")
                setisLogin({
                    status: true,
                    username: isLogin.username,
                    password: inputNewPasswd,
                    id: isLogin.id
                })
                setSuccess(true)
                setinputOldPasswd("")
                setinputNewPasswd("")
                setinputRetypeNewPasswd("")
            } else {
                // console.log("cek kembali isian anda")
                setFail(true)
            }
        } else {
            setFail(true)
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccess(false);
        setFail(false);
    };

    return (
        <Container>
            <Box mt="70px" m="auto" width="450px" >
                <Paper variant="outlined" elevation={1}>
                    <div style={{ margin: "32px" }}>
                        <Typography variant="h5" component="h2">
                            Change Password
                        </Typography>
                        <Typography variant="body2" align="left" color="textSecondary">
                            <br />
                        *Please complete the field.
                    </Typography>
                    </div>
                    <form>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Old Password"
                                type="password"
                                value={inputOldPasswd}
                                onChange={handleChangeOldPasswd}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockOpenIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="New Password"
                                type="password"
                                value={inputNewPasswd}
                                onChange={handleChangeNewPasswd}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Re-type New Password"
                                type="password"
                                value={inputRetypeNewPasswd}
                                onChange={handleChangeRetypeNewPasswd}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ marginTop: "16px", marginLeft: "32px", marginRight: "32px" }}>
                            <Button variant="contained" color="primary" fullWidth onClick={handleUpdatePasswd}>
                                Update
                            </Button>
                            <Snackbar anchorOrigin={{ vertical: "top", horizontal: "center" }} open={success} autoHideDuration={1000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="success">
                                    Successfully change password
                                </Alert>
                            </Snackbar>
                            <Snackbar anchorOrigin={{ vertical: "bottom", horizontal: "center" }} open={fail} autoHideDuration={1000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="warning">
                                    Please check again your input, make sure there's no empty field.
                                </Alert>
                            </Snackbar>
                        </div>
                    </form>
                    <br />
                </Paper>
            </Box>
        </Container>
    )
}

export default ChangePassword

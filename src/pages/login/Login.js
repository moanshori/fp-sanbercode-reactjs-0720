import React, { useState, useContext } from 'react';
import { Button, InputAdornment, Container, Box, TextField, Typography, Paper, Snackbar } from '@material-ui/core/';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import { LoginContext } from '../../context/LoginContext';
import MuiAlert from '@material-ui/lab/Alert';
import axios from 'axios';

import {
    useHistory
} from "react-router-dom";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Login() {
    let history = useHistory();
    const [inputUname, setinputUname] = useState("")
    const [inputPasswd, setinputPasswd] = useState("")
    const [isLogin, setisLogin] = useContext(LoginContext);
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);
    
    const handleChangeUname = (event) => {
        setinputUname(event.target.value);
    }
    const handleChangePasswd = (event) => {
        setinputPasswd(event.target.value);
    }

    const handleLogin = () => {
        if (inputUname !== "" && inputPasswd !== "") {
            axios.post(`https://backendexample.sanbersy.com/api/login`, { username: inputUname, password: inputPasswd })
                .then(res => {
                    // console.log("login")
                    // console.log(res.data)
                    setisLogin({
                        status: true,
                        username: inputUname,
                        password: inputPasswd,
                        id: res.data.id
                    })
                }, [])
            setSuccess(true)
            setinputUname("")
            setinputPasswd("")
            history.push("/home");
        } else {
            setFail(true)
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccess(false);
        setFail(false);
    };

    return (
        <Container>
            <Box mt="50px" m="auto" width="450px" >
                <Paper variant="outlined" elevation={1}>
                    <div style={{ margin: "32px" }}>
                        <Typography variant="h5" component="h2">
                            Login
                    </Typography>
                        <Typography variant="body2" align="left" color="textSecondary">
                            <br />
                        *Please complete the field.
                    </Typography>
                    </div>
                    <form>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Username"
                                value={inputUname}
                                onChange={handleChangeUname}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <AssignmentIndIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Password"
                                type="password"
                                value={inputPasswd}
                                onChange={handleChangePasswd}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockOpenIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ marginTop: "16px", marginLeft: "32px", marginRight: "32px" }}>
                            <Button variant="contained" color="primary" fullWidth onClick={handleLogin}>
                                Login
                            </Button>
                            <Snackbar open={success} autoHideDuration={1000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="success">
                                    Login Success.
                                </Alert>
                            </Snackbar>
                            <Snackbar open={fail} autoHideDuration={1000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="warning">
                                    Make sure there is no empty field.
                                </Alert>
                            </Snackbar>
                        </div>
                    </form>
                    <br />
                </Paper>
            </Box>
        </Container>
    )
}

export default Login
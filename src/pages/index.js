import React, { useState, useContext } from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { LoginContext } from '../context/LoginContext'
import Login from './login/Login'
import Home from './home/Home'
import SignUp from './login/SignUp'
import User from './home/User'
import About from './main/About'
import ChangePassword from './main/ChangePassword'
import ManageMovie from './main/ManageMovie'
import ManageGame from './main/ManageGame'
import AddGame from './main/AddGame'
import AddMovie from './main/AddMovie'

import { makeStyles } from '@material-ui/core/styles';
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    // BottomNavigation,
    // Paper,
    Snackbar,
    List,
    ListSubheader,
    Divider,
    ListItemIcon,
    ListItem,
    ListItemText,
    Menu,
    MenuItem,
    Drawer,
    // BottomNavigationAction,
} from '@material-ui/core/';
import AccountCircle from '@material-ui/icons/AccountCircle';
import HomeIcon from '@material-ui/icons/Home';
import EditIcon from '@material-ui/icons/Edit';
import QueueIcon from '@material-ui/icons/Queue';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import InfoIcon from '@material-ui/icons/Info';
import MuiAlert from '@material-ui/lab/Alert';
import EditGame from "./main/EditGame";
import EditMovie from "./main/EditMovie";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        // flexGrow: 1,
        display: 'flex',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    appBarBottom: {
        top: 'auto',
        bottom: 0,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerContainer: {
        overflow: 'auto',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    footer: {
        display: 'flex',
    }
}));

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Index = () => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const [isLogin, setisLogin] = useContext(LoginContext);
    const [logout, setLogout] = useState(false);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        setisLogin({
            status: false,
            username: ""
        })
        setLogout(true);
    }

    const handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setLogout(false);
    };

    return (
        <Router>
            <div className={classes.root}>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar variant="dense">
                        <Link to="/">
                            <IconButton edge="start" color="inherit" aria-label="menu">
                                <HomeIcon style={{ color: "white" }} />
                            </IconButton>
                        </Link>
                        <Typography variant="h6" className={classes.title}>
                            Final Project
                        </Typography>
                        <User name={isLogin.username} />
                        <IconButton color="default" aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            onClick={handleClick}>
                            <AccountCircle style={{ color: "white" }} />
                        </IconButton>
                        {isLogin.status === false && (
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}>
                                <MenuItem onClick={handleClose}><Link to="/login" style={{ color: 'inherit', textDecoration: 'inherit' }}>Login</Link></MenuItem>
                                <MenuItem onClick={handleClose}><Link to="/signup" style={{ color: 'inherit', textDecoration: 'inherit' }}>SignUp</Link></MenuItem>
                            </Menu>
                        )}
                    </Toolbar>
                </AppBar>

                {isLogin.status === true && (
                    <Drawer
                        className={classes.drawer}
                        variant="permanent"
                        classes={{
                            paper: classes.drawerPaper,
                        }}>
                        <Toolbar />
                        <div className={classes.drawerContainer}>
                            <List dense="true">
                                <ListSubheader style={{ textAlign: "left" }}>
                                    Movie
                                </ListSubheader>
                                <Link to="/managemovie" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon>
                                            <ListAltIcon style={{ color: "blue" }} />
                                        </ListItemIcon>
                                        <ListItemText primary="Manage" />
                                    </ListItem>
                                </Link>
                                <Link to="/movie/create" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon >
                                            <QueueIcon style={{ color: "blue" }} />
                                        </ListItemIcon>
                                        <ListItemText primary="Add" />
                                    </ListItem>
                                </Link>
                            </List>
                            <Divider />
                            <List dense="true">
                                <ListSubheader style={{ textAlign: "left" }}>
                                    Game
                                </ListSubheader>
                                <Link to="/managegame" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon>
                                            <ListAltIcon style={{ color: "lime" }} />
                                        </ListItemIcon>

                                        <ListItemText primary="Manage" />
                                    </ListItem>
                                </Link>
                                <Link to="/game/create" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon>
                                            <QueueIcon style={{ color: "lime" }} />
                                        </ListItemIcon>

                                        <ListItemText primary="Add" />
                                    </ListItem>
                                </Link>
                            </List>
                            <Divider />
                            <List dense="true">
                                <ListSubheader style={{ textAlign: "left" }}>
                                    Account
                                </ListSubheader>
                                <Link to="/changepassword" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon><EditIcon style={{ color: "brown" }} /></ListItemIcon>
                                        <ListItemText>Change Password</ListItemText>
                                    </ListItem>
                                </Link>
                                <Link to="/" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button onClick={handleLogout}>
                                        <ListItemIcon><ExitToAppIcon style={{ color: "brown" }} /></ListItemIcon>
                                        <ListItemText>Logout</ListItemText>
                                    </ListItem>
                                </Link>
                            </List>
                            <Divider />
                            <List dense="true">
                                <ListSubheader style={{ textAlign: "left" }}>
                                    Other
                                </ListSubheader>
                                <Link to="/about" style={{ color: 'inherit', textDecoration: 'inherit' }}>
                                    <ListItem button>
                                        <ListItemIcon><InfoIcon style={{ color: "greenyellow" }} />
                                        </ListItemIcon>
                                        <ListItemText>About</ListItemText>
                                    </ListItem>
                                </Link>
                            </List>
                        </div>
                    </Drawer>
                )}
                <main className={classes.content}>
                    <Switch >
                        <Route exact path="/managemovie" component={ManageMovie}></Route>
                        <Route path="/managemovie/edit/:id" component={EditMovie}></Route>
                        <Route exact path="/movie/create" component={AddMovie}></Route>
                        <Route exact path="/managegame" component={ManageGame}></Route>
                        <Route path="/managegame/edit/:id" component={EditGame}></Route>
                        <Route exact path="/game/create" component={AddGame}></Route>
                        <Route exact path="/signup" component={SignUp}></Route>
                        <Route exact path="/login" component={Login}></Route>
                        <Route path="/changepassword" component={ChangePassword}></Route>
                        <Route path="/about" component={About}></Route>
                        <Route path="/" component={Home}></Route>
                    </Switch>
                    <Snackbar open={logout} autoHideDuration={6000} onClose={handleCloseSnackBar}>
                        <Alert onClose={handleCloseSnackBar} severity="info">
                            You are logging out.
                        </Alert>
                    </Snackbar>
                    <footer>
                        <h5>copyright &copy; 2020 by Sanbercode</h5>
                    </footer>
                </main>
            </div>
        </Router >
    )
}

export default Index
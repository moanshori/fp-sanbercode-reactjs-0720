import React from "react"
import {
    Container,
} from '@material-ui/core/';
import ListGame from './ListGame';
import ListMovie from './ListMovie';

class Home extends React.Component {
    render() {
        return (
            <Container fixed>
                <ListMovie />
                <ListGame />
            </Container>
        )
    }
}

export default Home
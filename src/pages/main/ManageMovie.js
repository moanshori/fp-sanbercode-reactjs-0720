import React, { useState, useEffect } from "react"
import axios from 'axios'
import {
    Snackbar,
    Container,
    Box,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableHead,
    TableCell,
    Paper,
    Grid,
    IconButton,
    Button,
    Divider,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    DialogContentText,
    Typography,
    Slider,
    FormControl,
    InputLabel,
    Input,
    InputAdornment,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    TextField,
    MenuItem
} from '@material-ui/core/';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MuiAlert from '@material-ui/lab/Alert';
import SearchIcon from '@material-ui/icons/Search';
import TitleIcon from '@material-ui/icons/Title';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

import {
    useHistory
} from "react-router-dom";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function valuetext(value) {
    return `Year: ${value}`;
}

const ManageMovie = () => {
    let history = useHistory();
    const [dataMovie, setDataMovie] = useState([])
    const [buMovie, setbuMovie] = useState([])
    const [cond, setCond] = useState(true)
    const [movieTitle, setmovieTitle] = useState("")
    const [movieDesc, setmovieDesc] = useState("")
    const [movieReview, setmovieReview] = useState("")
    const [movieImage, setmovieImage] = useState("")
    const [openSnack, setOpenSnack] = useState(false);
    const [open, setOpen] = useState(false);
    const [inputSearch, setinputSearch] = useState("")
    const [openDialogDesc, setopenDialogDesc] = useState(false)
    const [openDialogRev, setopenDialogRev] = useState(false)
    const [inputFilterDura, setinputFilterDura] = useState('')
    const headTitle = ["Title", "Genre", "Rating", "Year", "Duration", "Description", "Review"]

    useEffect(() => {
        if (cond) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(res => {
                    setDataMovie(res.data.map(item => {
                        return {
                            id: item.id,
                            title: item.title,
                            description: item.description,
                            year: item.year,
                            duration: item.duration,
                            genre: item.genre,
                            rating: item.rating,
                            review: item.review,
                            image_url: item.image_url
                        }
                    }));
                });
            setCond(false)
        } else {
        }
        setbuMovie(dataMovie)
    }, [dataMovie])

    const handleLinkDialog = (event) => {
        console.log("link: " + event.currentTarget.value)

        dataMovie.map((item) => {
            if (item.id == event.currentTarget.value) {
                setmovieTitle(item.title)
                setmovieImage(item.image_url)
            }
        })
        setOpen(true);
    }

    const handleLinkDialogDesc = (event) => {
        console.log("dialog desc: " + event.currentTarget.value)

        dataMovie.map((item) => {
            if (item.id == event.currentTarget.value) {
                setmovieTitle(item.title)
                setmovieDesc(item.description)
            }
        })
        setopenDialogDesc(true);
    }

    const handleLinkDialogReview = (event) => {
        console.log("dialog rev: " + event.currentTarget.value)

        dataMovie.map((item) => {
            if (item.id == event.currentTarget.value) {
                setmovieTitle(item.title)
                setmovieReview(item.review)
            }
        })
        setopenDialogRev(true);
    }

    const handleClose = () => {
        setOpen(false);
        setOpenSnack(false)
        setopenDialogDesc(false)
        setopenDialogRev(false)
    };

    const handleChangeSearch = (event) => {
        setinputSearch(event.target.value);
    }

    const handleSearch = () => {
        setCond(true)
        if (inputSearch === "") {
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else {
            setbuMovie(dataMovie.filter(item => {
                if (item.title != null) {
                    return item.title.toLowerCase().includes(inputSearch.toLowerCase())
                }
            }))
        }
        setCond(false)
    }

    const handleEdit = (event) => {
        console.log("edit: " + event.currentTarget.value)
        history.push(`/managemovie/edit/${event.currentTarget.value}`);
        // console.log("work?")
    }

    const handleDelete = (event) => {
        let tempMovies = dataMovie.filter(item => item.id != event.currentTarget.value)
        console.log("delete: " + event.currentTarget.value)
        axios.delete(`https://backendexample.sanbersy.com/api/movies/${event.currentTarget.value}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
        setOpenSnack(true)
        setDataMovie([...tempMovies])
    }

    const handleChangeSlider = (event, value) => {
        setbuMovie(dataMovie.filter(item => {
            return item.year == value
        }))
    }

    const handleChangeSliderYear = (event, value) => {
        setbuMovie(dataMovie.filter(item => {
            return item.rating == value
        }))
    }

    const handleChangeFilterDura = (event) => {
        setinputFilterDura(event.target.value);
    }

    const handleFilterDura = () => {
        // console.log("filter dura")
        setbuMovie(dataMovie.filter(item => {
            return item.duration > inputFilterDura
        }))
    }

    const handleChangeSort = (event) => {
        console.log(event.target.value)
        if (event.target.value === "Title") {
            setbuMovie(dataMovie.sort(
                function (a, b) {
                    var nameA = a.title.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.title.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Genre") {
            setbuMovie(dataMovie.sort(
                function (a, b) {
                    var nameA = a.genre.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.genre.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Review") {
            setbuMovie(dataMovie.sort(
                function (a, b) {
                    var nameA = a.review.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.review.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Description") {
            setbuMovie(dataMovie.sort(
                function (a, b) {
                    var nameA = a.description.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.description.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Rating") {
            setbuMovie(dataMovie.sort(
                (a, b) => (a.rating) - (b.rating))
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Year") {
            setbuMovie(dataMovie.sort(
                setbuMovie(dataMovie.sort(
                    (a, b) => (a.year) - (b.year))
                ))
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Duration") {
            setbuMovie(dataMovie.sort(
                setbuMovie(dataMovie.sort(
                    (a, b) => (a.duration) - (b.duration))
                ))
            )
            const regexpCoordinates = /\w/g;
            setbuMovie(dataMovie.filter(item => {
                return item.title.toLowerCase().match(regexpCoordinates)
            }))
        }
    }

    return (
        <Container maxWidth="lg">
            <Box mt="50px">
                <Paper variant="outlined">
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="body2">Filter and Search</Typography>
                        </AccordionSummary>
                        <Divider />
                        <AccordionDetails>
                            <Grid container spacing={2}>
                                <Grid item xs={6} >
                                    <div style={{ margin: "20px" }}>
                                        <FormControl style={{ alignItems: "end", display: 'flex', justifyContent: 'end' }}>
                                            <InputLabel htmlFor="standard-adornment-password">Search by title:</InputLabel>
                                            <Input margin="dense" fullWidth
                                                value={inputSearch}
                                                onChange={handleChangeSearch}
                                                endAdornment={
                                                    <InputAdornment position="end">
                                                        <IconButton
                                                            onClick={handleSearch}
                                                        >
                                                            <SearchIcon />
                                                        </IconButton>
                                                    </InputAdornment>
                                                }
                                                startAdornment={
                                                    <InputAdornment position="start">
                                                        <TitleIcon />
                                                    </InputAdornment>
                                                }
                                            />
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={6} >
                                    <div style={{ margin: "20px" }}>
                                        <Typography align="left">
                                            Released Year
                                </Typography>
                                        <Slider
                                            onChange={handleChangeSlider}
                                            defaultValue={2020}
                                            getAriaValueText={valuetext}
                                            aria-labelledby="discrete-slider"
                                            valueLabelDisplay="on"
                                            step={1}
                                            marks
                                            min={1990}
                                            max={2020}
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={6} >
                                    <div style={{ margin: "20px" }}>
                                        <Typography align="left">
                                            Rating
                                </Typography>
                                        <Slider
                                            onChange={handleChangeSliderYear}
                                            defaultValue={10}
                                            getAriaValueText={valuetext}
                                            aria-labelledby="discrete-slider"
                                            valueLabelDisplay="on"
                                            step={1}
                                            marks
                                            min={1}
                                            max={10}
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div style={{ margin: "20px" }}>
                                        <FormControl style={{ alignItems: "end", display: 'flex', justifyContent: 'end' }}>
                                            <InputLabel htmlFor="standard-adornment-password">Duration more than theese minutes ...</InputLabel>
                                            <Input margin="dense" fullWidth
                                                inputMode="decimal"
                                                value={inputFilterDura}
                                                onChange={handleChangeFilterDura}
                                                endAdornment={
                                                    <InputAdornment position="end">
                                                        <IconButton
                                                            onClick={handleFilterDura}
                                                        >
                                                            <SearchIcon />
                                                        </IconButton>
                                                    </InputAdornment>
                                                }
                                                startAdornment={
                                                    <InputAdornment position="start">
                                                        <ArrowForwardIosIcon />
                                                    </InputAdornment>
                                                }
                                            />
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </AccordionDetails>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="body2">Sort</Typography>
                        </AccordionSummary>
                        <Divider />
                        <div style={{ margin: "20px", marginLeft: "100px", marginRight: "100px" }}>
                            <AccordionDetails>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Sort by:"
                                    select
                                    // value={item}
                                    onChange={handleChangeSort}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <CalendarTodayIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {headTitle.map((item) => (
                                        <MenuItem value={item}>
                                            {item}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </AccordionDetails>
                        </div>
                    </Accordion>
                </Paper>
                <br />
                <TableContainer component={Paper} variant="outlined">
                    <Table size="small" aria-label="a dense table" stickyHeader >
                        <TableHead>
                            <TableRow>
                                <TableCell align="right" sortDirection><b>Action</b></TableCell>
                                <TableCell ><b>Title</b></TableCell>
                                <TableCell ><b>Genre</b></TableCell>
                                <TableCell ><b>Rating</b></TableCell>
                                <TableCell ><b>Year</b></TableCell>
                                <TableCell ><b>Duration</b></TableCell>
                                <TableCell ><b>Description</b></TableCell>
                                <TableCell ><b>Review</b></TableCell>
                                <TableCell ><b>Image URL</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {buMovie.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell style={{ whiteSpace: 'nowrap', overflow: 'auto' }}>
                                        <IconButton aria-label="edit" value={row.id} onClick={handleEdit}>
                                            <EditIcon size="small" style={{ color: "blue" }} />
                                        </IconButton>
                                        <IconButton aria-label="delete" size="small" value={row.id} onClick={handleDelete}>
                                            <DeleteIcon style={{ color: "blue" }} />
                                        </IconButton>
                                    </TableCell>
                                    <TableCell style={{ overflow: 'auto' }}>{row.title}</TableCell>
                                    <TableCell style={{ overflowX: 'auto' }}>{row.genre}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.rating}</TableCell>
                                    <TableCell style={{ overflowX: 'auto' }}>{row.year}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.duration}</TableCell>
                                    <TableCell style={{ overflowX: 'auto' }}>
                                        {row.description != null && row.description.length > 10 ? (
                                            <div>
                                                {row.description.slice(0, 10)}
                                                <Button value={row.id} size="small" color="primary" style={{ textTransform: 'none' }} onClick={handleLinkDialogDesc}>
                                                    ...more
                                                        </Button>
                                            </div>
                                        ) : (<div>
                                            {row.description}
                                        </div>)}
                                    </TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
                                        {row.review != null && row.review.length > 10 ? (
                                            <div>
                                                {row.review.slice(0, 10)}
                                                <Button value={row.id} size="small" color="primary" style={{ textTransform: 'none' }} onClick={handleLinkDialogReview}>
                                                    ...more
                                                        </Button>
                                            </div>
                                        ) : (<div>
                                            {row.review}
                                        </div>)}
                                    </TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
                                        <Button value={row.id} variant="outlined" color="primary" size="small" onClick={handleLinkDialog}>
                                            Link
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Dialog
                    open={open}
                    onClose={handleClose}>
                    <DialogTitle>{movieTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <a href={movieImage} target="_blank">
                                {movieImage}
                            </a>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={openDialogDesc}
                    onClose={handleClose}>
                    <DialogTitle >{movieTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {movieDesc}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={openDialogRev}
                    onClose={handleClose}>
                    <DialogTitle >{movieTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {movieReview}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={openSnack} autoHideDuration={1000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="info">
                        Delete sucessful.
                    </Alert>
                </Snackbar>
            </Box >
        </Container >
    )
}

export default ManageMovie
import React, { useState } from 'react';
import { Snackbar, Button, InputAdornment, Container, Box, TextField, Typography, Paper } from '@material-ui/core/';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import axios from 'axios';
import MuiAlert from '@material-ui/lab/Alert';

import {
    useHistory
} from "react-router-dom";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SignUp() {
    let history = useHistory();
    const [inputUname, setinputUname] = useState("")
    const [inputPasswd, setinputPasswd] = useState("")
    const [success, setSuccess] = useState(false);
    const [fail, setFail] = useState(false);

    const handleChangeUname = (event) => {
        setinputUname(event.target.value);
    }
    const handleChangePasswd = (event) => {
        setinputPasswd(event.target.value);
    }

    const handleLogin = (event) => {
        event.preventDefault()
        let data = {
            username: inputUname,
            password: inputPasswd
        }
        if (inputUname !== "" && inputPasswd !== "") {
            axios.post(`https://backendexample.sanbersy.com/api/users`, data)
                .then(res => {
                    console.log("signup")
                    console.log(res.data);
                })
            setSuccess(true)
            history.push("/home");
        } else {
            setFail(true)
        }
        setinputUname("")
        setinputPasswd("")
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccess(false);
        setFail(false);
    };

    return (
        <Container>
            <Box mt="50px" m="auto" width="450px" >
                <Paper variant="outlined" elevation={1}>
                    <div style={{ margin: "32px" }}>
                        <Typography variant="h5" component="h2">
                            SignUp
                    </Typography>
                        <Typography variant="body2" align="left" color="textSecondary">
                            <br />
                        *Please complete the field.
                    </Typography>
                    </div>
                    <form>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Username"
                                value={inputUname}
                                onChange={handleChangeUname}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <AssignmentIndIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ margin: "32px" }}>
                            <TextField
                                fullWidth
                                id="input-with-icon-textfield"
                                label="Password"
                                type="password"
                                value={inputPasswd}
                                onChange={handleChangePasswd}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <LockOpenIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </div>
                        <div style={{ marginTop: "16px", marginLeft: "32px", marginRight: "32px" }}>
                            <Button variant="contained" color="primary" fullWidth onClick={handleLogin}>
                                SignUp
                            </Button>
                            <Snackbar open={success} autoHideDuration={6000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="success">
                                    SignUp Success. Now you can log in to access all feature.
                                </Alert>
                            </Snackbar>
                            <Snackbar open={fail} autoHideDuration={6000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="warning">
                                    Please check the field again, make sure there is no empty field.
                                </Alert>
                            </Snackbar>
                        </div>
                    </form>
                    <br />
                </Paper>
            </Box>
        </Container>
    )
}

export default SignUp
import React, { useState, useEffect } from "react"
import axios from 'axios'
import {
    Snackbar,
    Container,
    Box,
    TableContainer,
    Table,
    TableBody,
    TableRow,
    TableHead,
    TableCell,
    Paper,
    IconButton,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    DialogContentText,
    Typography,
    FormControlLabel,
    TextField,
    Grid,
    FormControl,
    FormLabel,
    MenuItem,
    Select,
    Slider,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Divider,
    InputLabel,
    Input,
    Radio,
    RadioGroup,
    InputAdornment
} from '@material-ui/core/';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MuiAlert from '@material-ui/lab/Alert';
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';

import {
    useHistory
} from "react-router-dom";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
function valuetext(value) {
    return `Year: ${value}`;
}
const ManageGame = () => {
    let history = useHistory();
    const [dataGame, setDataGame] = useState([])
    const [open, setOpen] = useState(false);
    const [openSnack, setOpenSnack] = useState(false);
    const [gameImage, setgameImage] = useState("")
    const [gameName, setgameName] = useState("")
    const [inputSearch, setinputSearch] = useState("")
    const [buGame, setbuGame] = useState([])
    const [cond, setCond] = useState(true)
    const [value, setValue] = useState('all');
    const [genres] = useState([
        "", "Action", "Anime", "Arcade", "Board", "Casual", "Racing", "Tactic", "Sport"
    ])
    const headTitle = ["Name", "Genre", "Release", "Platform", "SinglePlayer", "MultiPlayer"]

    useEffect(() => {
        if (cond) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setDataGame(res.data.map(item => {
                        return {
                            id: item.id,
                            name: item.name,
                            genre: item.genre,
                            singlePlayer: item.singlePlayer,
                            multiplayer: item.multiplayer,
                            platform: item.platform,
                            release: item.release,
                            image_url: item.image_url
                        }
                    }));
                });
            setCond(false)
        } else {
        }
        setbuGame(dataGame)
    }, [dataGame])

    const handleEditGame = (event) => {
        console.log("edit: " + event.currentTarget.value)
        history.push(`/managegame/edit/${event.currentTarget.value}`);
    }

    const handleChangeSlider = (event, value) => {
        setbuGame(dataGame.filter(item => {
            return item.release == value
        }))
    }

    const handleDeleteGame = (event) => {
        let tempGame = dataGame.filter(item => item.id != event.currentTarget.value)
        console.log("delete: " + event.currentTarget.value)
        axios.delete(`https://backendexample.sanbersy.com/api/games/${event.currentTarget.value}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
        setOpenSnack(true)
        setDataGame([...tempGame])
    }

    const handleLinkDialog = (event) => {
        console.log("link: " + event.currentTarget.value)

        dataGame.map((item) => {
            if (item.id == event.currentTarget.value) {
                setgameImage(item.image_url)
                setgameName(item.name)
            }
        })
        setOpen(true);
    }
    const handleClose = () => {
        setOpen(false);
        setOpenSnack(false)
    };

    const handleChangeSearch = (event) => {
        setinputSearch(event.target.value);
    }

    const handleChangeRadio = (event) => {
        setValue(event.target.value)
        console.log(event.target.value)
        if (event.target.value == "sp") {
            // console.log("run sp")
            setbuGame(dataGame.filter(item => {
                return item.singlePlayer == 1
            }))
        } else if (event.target.value == "mp") {
            // console.log("run mp")
            setbuGame(dataGame.filter(item => {
                return item.multiplayer == 1
            }))
        } else {
            // console.log("run all")
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        }
    }

    const handleChangeGenre = (event) => {
        // console.log(event.target.value)
        setbuGame(dataGame.filter(item => {
            return item.genre.toLowerCase().match(event.target.value.toLowerCase())
        }))
    }

    const handleSearch = () => {
        setCond(true)
        // console.log("search: " + inputSearch)
        // console.log("bugame length search" + buGame.length)
        if (inputSearch === "") {
            // console.log("run")
            // console.log("game bu " + buGame.length)
            // console.log("game data " + dataGame.length)
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                // console.log(item.name)
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else {
            // console.log("else game bu " + buGame.length)
            setbuGame(dataGame.filter(item => {
                // console.log(item.name)
                if (item.name != null) {
                    return item.name.toLowerCase().includes(inputSearch.toLowerCase())
                }
            }))
        }
        setCond(false)
    }

    const handleChangeSort = (event) => {
        console.log(event.target.value)
        if (event.target.value === "Name") {
            setbuGame(dataGame.sort(
                function (a, b) {
                    var nameA = a.name.toUpperCase();
                    var nameB = b.name.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Genre") {
            setbuGame(dataGame.sort(
                function (a, b) {
                    var nameA = a.genre.toUpperCase();
                    var nameB = b.genre.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Platform") {
            setbuGame(dataGame.sort(
                function (a, b) {
                    var nameA = a.platform.toUpperCase();
                    var nameB = b.platform.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                })
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "Release") {
            setbuGame(dataGame.sort(
                (a, b) => (a.release) - (b.release))
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "SinglePlayer") {
            setbuGame(dataGame.sort(
                setbuGame(dataGame.sort(
                    (a, b) => (a.singlePlayer) - (b.singlePlayer))
                ))
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        } else if (event.target.value === "MultiPlayer") {
            setbuGame(dataGame.sort(
                setbuGame(dataGame.sort(
                    (a, b) => (a.multiplayer) - (b.multiplayer))
                ))
            )
            const regexpCoordinates = /\w/g;
            setbuGame(dataGame.filter(item => {
                return item.name.toLowerCase().match(regexpCoordinates)
            }))
        }
    }

    return (
        <Container maxWidth="lg">
            <Box mt="50px">
                <Paper variant="outlined">
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="body2">Filter and Search</Typography>
                        </AccordionSummary>
                        <Divider />
                        <Grid container spacing={2}>
                            <Grid item xs={6} >
                                <div style={{ margin: "20px" }}>
                                    <FormControl variant="outlined" style={{ alignItems: "end", display: 'flex', justifyContent: 'end' }}>
                                        <InputLabel >Search name ...</InputLabel>
                                        <Input margin="dense" fullWidth
                                            value={inputSearch}
                                            onChange={handleChangeSearch}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        onClick={handleSearch}
                                                    >
                                                        <SearchIcon />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                            </Grid>
                            <Grid item xs={6} >
                                <div style={{ margin: "20px" }}>
                                    <Typography align="left">
                                        Released Year
                            </Typography>
                                    <Slider
                                        onChange={handleChangeSlider}
                                        defaultValue={2020}
                                        getAriaValueText={valuetext}
                                        aria-labelledby="discrete-slider"
                                        valueLabelDisplay="on"
                                        step={1}
                                        marks
                                        min={1990}
                                        max={2020}
                                    />
                                </div>
                            </Grid>
                            <Grid item xs={6} >
                                <div style={{ margin: "20px" }}>
                                    <FormControl component="fieldset">
                                        <FormLabel component="legend">Single/Multiplayer</FormLabel>
                                        <RadioGroup aria-label="SMPlayer" name="player" value={value} onChange={handleChangeRadio}>
                                            <FormControlLabel value="sp" control={<Radio />} label="SinglePlayer" />
                                            <FormControlLabel value="mp" control={<Radio />} label="MultiPlayer" />
                                            <FormControlLabel value="all" control={<Radio />} label="Both" />
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                            </Grid>
                            <Grid item xs={6}>
                                <div style={{ margin: "20px" }}>
                                    <FormControl variant="outlined" fullWidth>
                                        <InputLabel align="left">Genre</InputLabel>
                                        <Select
                                            // value={g}
                                            onChange={handleChangeGenre}
                                            label="Genre">
                                            {genres.map((item) => (
                                                <MenuItem value={item}>{item}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div>
                            </Grid>
                        </Grid>
                    </Accordion>
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="body2">Sort</Typography>
                        </AccordionSummary>
                        <Divider />
                        <div style={{ margin: "20px", marginLeft: "100px", marginRight: "100px" }}>
                            <AccordionDetails>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Sort by:"
                                    select
                                    onChange={handleChangeSort}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SortByAlphaIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {headTitle.map((item) => (
                                        <MenuItem value={item}>
                                            {item}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </AccordionDetails>
                        </div>
                    </Accordion>
                </Paper>
                <br />
                <TableContainer component={Paper} variant="outlined">
                    <Table size="small" aria-label="a dense table" stickyHeader >
                        <TableHead>
                            <TableRow>
                                <TableCell align="right"><b>Action</b></TableCell>
                                <TableCell>
                                    <b>Name</b>
                                </TableCell>
                                <TableCell ><b>Genre</b></TableCell>
                                <TableCell ><b>Released</b></TableCell>
                                <TableCell ><b>Platform</b></TableCell>
                                <TableCell ><b>SinglePlayer</b></TableCell>
                                <TableCell ><b>MultiPlayer</b></TableCell>
                                <TableCell ><b>Image URL</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {buGame.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell style={{ whiteSpace: 'nowrap', overflow: 'auto' }}>
                                        <IconButton aria-label="edit" value={row.id} onClick={handleEditGame}>
                                            <EditIcon size="small" style={{ color: "blue" }} />
                                        </IconButton>
                                        <IconButton aria-label="delete" size="small" value={row.id} onClick={handleDeleteGame}>
                                            <DeleteIcon style={{ color: "blue" }} />
                                        </IconButton>
                                    </TableCell>
                                    <TableCell scope="row" style={{ overflow: 'auto' }}>{row.name} </TableCell>
                                    <TableCell style={{ overflow: 'auto' }}>{row.genre}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.release}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.platform}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.singlePlayer}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{row.multiplayer}</TableCell>
                                    <TableCell style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
                                        <Button value={row.id} variant="outlined" color="primary" size="small" onClick={handleLinkDialog}>
                                            Link
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Dialog
                    open={open}
                    onClose={handleClose}>
                    <DialogTitle id="alert-dialog-title">{gameName}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {gameImage}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={openSnack} autoHideDuration={1000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="info">
                        Delete sucessful.
                    </Alert>
                </Snackbar>
            </Box >
        </Container>
    )
}

export default ManageGame
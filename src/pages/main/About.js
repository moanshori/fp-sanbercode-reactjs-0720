import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import logo from '../../assets/twitter_header_photo_1.png'
import {
    Card,
    CardActions,
    CardActionArea,
    CardMedia,
    CardContent,
    Button,
    Typography,
    Container,
    Box
} from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        marginBottom: theme.spacing(1),
    },
}));

function About() {
    const classes = useStyles();

    return (
        <Container fixed>
            <Box mt="70px" mr="20px" ml="20px">
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia component={'img'}
                            image={logo}
                            title="Logo saya"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                Mochamad Anshori
                        </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Mengikuti training yang di selenggarakan oleh Sanbercode adalah salah satu solusi mengisi waktu selama dirumah.
                                Selain itu juga dapat memperlebar sayap tentang IT supaya dapat menjangkau berbagai macam bidang lain yang belum dikuasai.
                                Terima kasih banyak Sanbercode, terutama trainer yang ulet sekali :)
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Box>
        </Container>
    )
}

export default About
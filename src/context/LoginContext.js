import React, { useState, createContext } from "react";

export const LoginContext = createContext();
export const LoginProvider = props => {
    const [isLogin, setisLogin] = useState({
        // status: true,
        // username: "uyee",
        // password: "yowman",
        // id: 152
        status: false,
        username: "",
        password: "",
        id: 0
    })

    return (
        <LoginContext.Provider value={[isLogin, setisLogin]}>
            {props.children}
        </LoginContext.Provider>
    )
} 
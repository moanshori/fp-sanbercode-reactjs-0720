import React, { useState } from "react"
import axios from 'axios';
import {
    Container,
    Box,
    Paper,
    InputAdornment,
    TextField,
    Grid,
    MenuItem,
    Snackbar,
    Button,
    Typography
} from '@material-ui/core/';
import ImageIcon from '@material-ui/icons/Image';
import TitleIcon from '@material-ui/icons/Title';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SortIcon from '@material-ui/icons/Sort';
import CodeIcon from '@material-ui/icons/Code';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import SaveIcon from '@material-ui/icons/Save';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const AddGame = () => {
    const [gameName, setgameName] = useState("")
    const [gameImage, setgameImage] = useState("")
    const [gameGenre, setgameGenre] = useState("")
    const [gamePlatform, setgamePlatform] = useState("")
    const [gameRelease, setgameRelease] = useState("")
    const [gameSingle, setgameSingle] = useState("")
    const [gameMulti, setgameMulti] = useState("")
    const [success, setSuccess] = useState(false);

    const years = Array.from(new Array(50), (val, index) => new Date().getFullYear() - index);
    const option = [{
        value: 0,
        label: "No"
    },
    {
        value: 1,
        label: "Yes"
    }]

    const handleChangeName = (event) => {
        setgameName(event.target.value);
    }
    const handleChangeGenre = (event) => {
        setgameGenre(event.target.value);
    }
    const handleChangeRelease = (event) => {
        setgameRelease(event.target.value);
    }
    const handleChangePlatform = (event) => {
        setgamePlatform(event.target.value);
    }
    const handleChangeImageUrl = (event) => {
        setgameImage(event.target.value);
    }
    const handleChangeSingleP = (event) => {
        setgameSingle(event.target.value);
    }
    const handleChangeMultiP = (event) => {
        setgameMulti(event.target.value);
    }

    const handleAdd = () => {
        console.log("adding data")
        let data = {
            name: gameName,
            genre: gameGenre,
            singlePlayer: gameSingle == "Yes" ? 1 : 0,
            multiplayer: gameMulti == "Yes" ? 1 : 0,
            platform: gamePlatform,
            release: gameRelease,
            image_url: gameImage
        }
        console.log(data)
        axios.post(`https://backendexample.sanbersy.com/api/games`, data)
            .then(res => {
                console.log("Add new game")
                console.log(res.data)
                setSuccess(true)
            })
        setgameImage("");
        setgameGenre("");
        setgameRelease("");
        setgamePlatform("")
        setgameImage("");
        setgameSingle("");
        setgameMulti("");
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSuccess(false);
    };

    return (
        <Container fixed>
            <Box mt="80px" mr="20px" ml="20px">
                <Paper variant="outlined" elevation={1} >
                    <div style={{marginTop: "25px"}}>
                        <Typography variant="h5" component="h2">
                            Form Add Game Data
                    </Typography>
                    </div>
                    <Grid container spacing={1} justify="center" style={{ marginTop: "10px", marginBottom: "20px" }}>
                        <Grid item xs={6} >
                            <div style={{ marginTop: "20px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Name:"
                                    value={gameName}
                                    onChange={handleChangeName}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <TitleIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6} >
                            <div style={{ marginTop: "20px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Genre:"
                                    value={gameGenre}
                                    onChange={handleChangeGenre}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SortIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Release year:"
                                    select
                                    value={gameRelease}
                                    onChange={handleChangeRelease}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <CalendarTodayIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {years.map((year) => (
                                        <MenuItem value={year}>
                                            {year}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Platform:"
                                    value={gamePlatform}
                                    onChange={handleChangePlatform}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <CodeIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="SinglePlayer:"
                                    select
                                    value={gameSingle}
                                    onChange={handleChangeSingleP}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SupervisorAccountIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {option.map((val) => (
                                        <MenuItem value={val.label}>
                                            {val.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div style={{ marginTop: "10px", marginRight: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="MultiPlayer:"
                                    select
                                    value={gameMulti}
                                    onChange={handleChangeMultiP}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <SupervisedUserCircleIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                >
                                    {option.map((val) => (
                                        <MenuItem value={val.label}>
                                            {val.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div style={{ marginTop: "10px", marginRight: "50px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <TextField fullWidth
                                    autoComplete='off'
                                    label="Image Url:"
                                    value={gameImage}
                                    onChange={handleChangeImageUrl}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <ImageIcon />
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div style={{ margin: "10px", marginRight: "50px", marginLeft: "50px", paddingLeft: "35px", paddingRight: "35px" }}>
                                <Button
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={handleAdd}
                                    startIcon={<SaveIcon />}>
                                    Submit
                                    </Button>
                            </div>
                        </Grid>
                    </Grid>
                </Paper>
                <Snackbar open={success} autoHideDuration={1000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        New data sucessfully added.
                    </Alert>
                </Snackbar>
            </Box>
        </Container>
    )
}

export default AddGame
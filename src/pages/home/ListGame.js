import React, { useState, useEffect } from "react"
import axios from 'axios'
import {
    Typography,
    Paper,
    Box,
    List,
    ListItem,
    ListItemIcon,
    ListItemAvatar,
    ListItemText,
    makeStyles,
    ListItemSecondaryAction,
    IconButton,
    Grid,
    CardContent,
    Card,
    CardMedia,
} from '@material-ui/core/';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ImageIcon from '@material-ui/icons/Image';
import TitleIcon from '@material-ui/icons/Title';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SortIcon from '@material-ui/icons/Sort';
import CodeIcon from '@material-ui/icons/Code';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        whiteSpace: 'nowrap',
        marginBottom: theme.spacing(1),
    },
}));

function ListGame() {
    const classes = useStyles();
    const [dataGame, setDataGame] = useState([])
    const [gameName, setgameName] = useState("")
    const [gameImage, setgameImage] = useState("")
    const [gameGenre, setgameGenre] = useState("")
    const [gamePlatform, setgamePlatform] = useState("")
    const [gameRelease, setgameRelease] = useState("")
    const [gameSingle, setgameSingle] = useState(0)
    const [gameMulti, setgameMulti] = useState(0)
    const [show, setShow] = useState(false)

    const handleMoreGame = (event) => {
        console.log(event.currentTarget.value)
        setShow(true)

        axios.get(`https://backendexample.sanbersy.com/api/games/${event.currentTarget.value}`)
            .then(res => {
                setgameName(res.data.name)
                setgameImage(res.data.image_url)
                setgameGenre(res.data.genre)
                setgamePlatform(res.data.platform)
                setgameRelease(res.data.release)
                setgameSingle(res.data.singlePlayer)
                setgameMulti(res.data.multiplayer)
            });
    }

    useEffect(() => {
        if (dataGame.length == 0) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(res => {
                    setDataGame(res.data.map(item => {
                        return {
                            id: item.id,
                            name: item.name,
                            genre: item.genre,
                            singlePlayer: item.singlePlayer,
                            multiplayer: item.multiplayer,
                            platform: item.platform,
                            release: item.release,
                            image_url: item.image_url
                        }
                    }));
                });
        }
    }, [dataGame])

    return (
        <Box mt="30px" mr="10px" ml="10px">
            <Typography variant="h6" align="left">
                Game List
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={6} >
                    <Paper variant="outlined" elevation={1} className={classes.paper}>
                        <List dense="true" style={{ maxHeight: 450, overflow: 'auto' }}>
                            {dataGame.map((row) => (
                                <ListItem>
                                    <ListItemAvatar>
                                        <SportsEsportsIcon style={{ color: "lime" }} />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={`${row.name} (${row.release})`}
                                        secondary={row.genre}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="more" value={row.id} onClick={handleMoreGame}>
                                            <MoreVertIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Card variant="outlined" elevation={1} style={{ maxHeight: 480, overflow: 'auto' }}>
                        {show == true && (
                            <>
                                <Box ml="10px" mt="10px">
                                    <Typography variant="body2" align="left" color="textSecondary">
                                        Game Details
                                    </Typography>
                                </Box>
                                <CardContent>
                                    <CardMedia component={'img'}
                                        image={gameImage}>
                                    </CardMedia>
                                    <List dense="true">
                                        {/* <ListItem>
                                            <ListItemIcon>
                                                <ImageIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left" style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}
                                                primary={gameImage}
                                            />
                                        </ListItem> */}
                                        <ListItem>
                                            <ListItemIcon>
                                                <TitleIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={gameName}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <SortIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={gameGenre}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <CodeIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={gamePlatform}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <CalendarTodayIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={gameRelease}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemIcon>
                                                <SupervisorAccountIcon />
                                            </ListItemIcon>
                                            <ListItemText align="left"
                                                primary={
                                                    gameSingle == 0 ? "MultiPlayer" : "SinglePlayer"
                                                }
                                            />
                                        </ListItem>
                                    </List>
                                </CardContent>
                            </>
                        )}
                        {show == false && (
                            <Box m="20px">
                                <Typography variant="body2" align="left" color="textSecondary" padding="15px">
                                    Select one of the lists beside to see the details.
                            </Typography>
                            </Box>
                        )}
                    </Card>
                </Grid>
            </Grid>
        </Box>
    )
}

export default ListGame